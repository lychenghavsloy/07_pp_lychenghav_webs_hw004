import React, { Component } from "react";
import Swal from 'sweetalert2'
import 'animate.css';
export default class TableComponent extends Component {
  onAlert = (mydata) => {
    Swal.fire({
      title: `ID : ${mydata.id} \n Email : ${mydata.Email1} \n Username : ${mydata.NameUser} \n Age : ${mydata.Age1}`,
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    })
}
  render() {
    return (
      <div>
        <div className="mt-10 w-4/4 flex justify-center  items-center">
          <table>
            <thead class="text-xs text-gray-700 bg-orange-300 uppercase font-hi dark:bg-gray-700 dark:text-gray-400">
              <tr>
                <th scope="col" class=" px-12 py-3 text-black">
                  ID
                </th>
                <th scope="col" class="px-20 py-3 text-black">
                  E-mail
                </th>
                <th scope="col" class="px-16 py-3 text-black">
                  Username
                </th>
                <th scope="col" class="px-12 py-3 text-black">
                  Age
                </th>
                <th colSpan={2} scope="col" class=" px-28 py-3 text-black">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.mydata.map((i) => (
                <tr className=" font-a1 odd:bg-white even:bg-gray-300" key={i.id}>
                  <td class="px-12 py-3 text-black">{i.id}</td>
                  <td class=" px-16 py-3 text-black">{i.Email1}</td>
                  <td class="px-16 py-3 text-black">{i.NameUser}</td>
                  <td class="px-20 py-3 text-black">{i.Age1}</td>
                  
                  <td>
                    {" "}
                    <button
                      type="button"
                        class= {i.Status === "Pending" ? "inline-block rounded shadow-md shadow-red-300 bg-red-500 px-6 pt-2.5 pb-2 text-xs font-medium uppercase leading-normal text-white w-28" : "inline-block w-28 rounded shadow-md shadow-red-300 bg-orange-300 px-6 pt-2.5 pb-2 text-xs font-medium uppercase leading-normal text-white"
              }
                      onClick ={ () => this.props.onChangePending(i)}
                    
                    >
                      {i.Status}
                    </button>
                  </td>
                  <td>
                    {" "}
                    <button
                      type="button"
                      class="inline-block rounded  bg-blue-500 px-6 pt-2.5 pb-2 text-xs font-medium uppercase leading-normal shadow-md shadow-blue-300 text-white"
                      onClick={() => this.onAlert(i)}
                    >
                      Show More
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
