import React, { Component } from 'react'
import TableComponent from './TableComponent'

export default class InputComponent extends Component {

constructor(){
    super();
    this.state = {
        User: [
        {id: 1, Email1:"lychenghav@gmail.com", NameUser:"Ly Chenghav", Age1: 21, Status: "Pending"},
        {id: 2, Email1:"Rithysak@gmail.com", NameUser:"Ren Rithysak", Age1: 21, Status: "Pending"},
        {id: 3, Email1:"Sovannak@gmail.com", NameUser:"Kheng Sovannak", Age1: 21, Status: "Pending"},
    ],
    newEmail: "",
    newUser: "",
    newAge: "",

};
}

handleEmail = (e) =>{
    this.setState({newEmail: e.target.value,});
}

handleUsername = (e) =>{
    this.setState({newUser: e.target.value,});
}

handleAge = (e) =>{
    this.setState({newAge: e.target.value,});
}

handlePending = (e) =>{
    const User = this.state.User;
    for (let i = 0; i < User.length; i++) {
        if(User[i].id === e.id){
            e.Status = e.Status === "Pending"? "Done" : "Pending";
        }
    }
    this.setState({...this.state.User})

}

onSubmit = () => {
    const newOBJ ={
        id : this.state.User.length + 1,
        Email1 : this.state.newEmail,
        NameUser : this.state.newUser,
        Age1 : this.state.newAge,
    };
    const usingSpread = {...newOBJ}
    this.setState(
        {
            User : [...this.state.User, usingSpread],
            newEmail: "",
            newUser: "",
            newAge : "",
        },
        () => console.log(this.state.newAge)
    ); 
};



  render() {
    return (
      <div>
        <h1 className='font-extrabold text-transparent mt-3 text-center font-lob text-6xl bg-clip-text bg-gradient-to-r from-green-500 via-purple-400 to-pink-600'>
        Please fill your information
        </h1>
        <section class= "">
  <div class="flex flex-col items-center font-hi w-3/4 justify-center px-6 py-5 mx-auto">
      <div class="w-full rounded-lg ">
          <div class="p-6 space-y-4">
        
              <form class="space-y-4 md:space-y-6" action="#">
                  <div>
                      <label for="email" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your email</label>
                      <input 
                
                      type="email" name="email" id="email" class="bg-gray-50 border shadow-md shadow-orange-300 border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="사랑해요@gmail.com" required="" onChange={this.handleEmail}/>
                  </div>
                  <div>
                      <label for="Text" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Username</label>
                      <input type="text" name="password" id="password" placeholder="......" class="bg-gray-50 shadow-md shadow-orange-300 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="" onChange={this.handleUsername}/>
                  </div>
                  <div>
                      <label for="text" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Age</label>
                      <input type="text" name="confirm-password" id="confirm-password" placeholder="❤️" class="bg-gray-50 shadow-md shadow-orange-300 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="" onChange={this.handleAge}/>
                  </div>
                  <div class="flex items-start">
                     
                  </div>
                
              </form>
          </div>
      </div>
  </div>
</section>
<div className='flex justify-center items-center'>
<button onClick={this.onSubmit} class="px-6 py-2 w-1/4 text-white font-lob container mx-auto rounded bg-gradient-to-r from-red-800 via-yellow-600 to-yellow-500">Register</button>
</div>
{/* <TableComponent/> */}
<TableComponent mydata = {this.state.User} onChangePending={this.handlePending}/>
</div>

    )
  }
}
