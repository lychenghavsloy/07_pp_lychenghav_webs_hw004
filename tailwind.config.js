/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
    },
    fontFamily:{
      hi : ['Libre Baskerville', 'serif'],
      lob : ['Lobster', 'cursive'],
      asor : ['Barlow Condensed', 'sans-serif'],
      a1 : ['Signika', 'sans-serif'],
    }
  },
  plugins: [],
}